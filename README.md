# CoAppExamFrontend

## Requirements
1. NodeJS
2. Yarn
3. Backend (CoAppExam)[https://gitlab.com/julio.telan/CoAppExam]

## Setup
Install all the dependencies through `yarn install`

### Running the project for development
1. `yarn start`

### Running the project for production
1. `yarn build`
2. `serve build/`