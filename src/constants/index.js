export const LIST_PEOPLE = 'people/LIST_PEOPLE';
export const GET_PERSON = 'people/GET_PERSON';
export const SELECT_PERSON = 'people/SELECT_PERSON';
export const SHOW_MODAL_NOW = 'people/SHOW_MODAL_NOW';
export const HIDE_MODAL_NOW = 'people/HIDE_MODAL_NOW';

export const API_BASE_URL = 'http://localhost:9292/api';
