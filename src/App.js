import React, { Component } from 'react';
import AppContainer from './containers/';
import './App.css';
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import store, { history } from './store'
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <div>
            <AppContainer />
          </div>
        </ConnectedRouter>
      </Provider>
    )
  }
}

export default App;
