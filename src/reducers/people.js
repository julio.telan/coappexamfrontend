import { LIST_PEOPLE, GET_PERSON, SHOW_MODAL_NOW, HIDE_MODAL_NOW, SELECT_PERSON } from '../constants';

const initialState = {
  people: [],
  metadata: {},
  selectedPerson: {},
  showModal: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case LIST_PEOPLE:
      return {
        ...state,
        people: action.people,
        metadata: action.metadata,
        showModal: false
      };
    case GET_PERSON:
    case SELECT_PERSON:
      return {
          ...state,
          selectedPerson: action.selectedPerson,
          showModal: true
      };
    
    case SHOW_MODAL_NOW:
      return {
        ...state,
        showModal: true
      }
    case HIDE_MODAL_NOW:
      return {
        ...state,
        showModal: false
      }
    default:
      return state
  }
}