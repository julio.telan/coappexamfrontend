import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { 
  Table, 
  Button, 
  Modal, 
  FormControl, 
  FormGroup, 
  ControlLabel 
} from 'react-bootstrap';
import {
  listPeopleNow,
  createPersonNow,
  showModalNow,
  hideModalNow,
  deletePersonNow,
  selectPersonNow
} from '../actions/home';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      people: props.people,
      metadata: props.metadata,
      selectedPerson: props.selectedPerson,
      showModal: props.showModal
    }
  }

  componentDidMount() {
    this.props.listPeopleNow(1, 30);
  }

  componentWillReceiveProps(nextProps, _nextContext) {
    this.setState({...nextProps})
  }

  renderData = (data) => {
    return data.map((person) => {
      return (<tr 
        key={"person_row_" + person._id}
        onClick={() => this.props.selectPersonNow(person)}
        >
        <td>{person.first_name}</td>
        <td>{person.last_name}</td>
        <td>{person.contact_number}</td>
        <td><Button bsStyle="danger" onClick={() => this.props.deletePersonNow(person._id)}>x</Button></td>
      </tr>)
    })
  }
  
  onClickAdd = () => {
    this.setState({showModal: true});
  }

  onHideModal = () => {
    this.setState({showModal: false});
  }

  render() {
    return (
      <div className="container" style={{paddingTop: 30}}>
        <div className="row">
          <div className="col-md-11 col-sm-11" style={{paddingLeft: 0}}>
            <p style={{fontSize: 25}}>People</p>
          </div>
          <div className="col-md-1 col-sm-1">
            <Button bsStyle="success" style={{marginLeft: "-6px"}} onClick={() => this.props.showModalNow()}>+</Button>
          </div>
          
        </div>

        <div className="row">
          <Table striped hover>
            <thead>
              <tr>
                <th className="col-md-4 col-sm-4">First Name</th>
                <th className="col-md-4 col-sm-4">Last Name</th>
                <th className="col-md-3 col-sm-3">Contact Number</th>
                <th className="col-md-1 col-sm-1">Delete</th>
              </tr>
            </thead>
            <tbody>
              {this.renderData(this.state.people)}
            </tbody>
          </Table>
        </div>
        <PersonModal onHideModal={this.props.hideModalNow.bind(this)} 
          showModal={this.props.showModal} 
          submitForm={this.props.createPersonNow.bind(this)}
          selectedPerson={this.props.selectedPerson} />
      </div>
    )
  }
}

class PersonModal extends React.Component {
  constructor(props){
    super(props);
    console.warn(props);
    
    const selectedPerson = props.selectedPerson;
    this.state = {
      first_name: selectedPerson ? selectedPerson.first_name : "",
      last_name: selectedPerson ? selectedPerson.last_name : "",
      contact_number: selectedPerson ? selectedPerson.contact_number : "",
      showModal: false,
      submitForm: props.submitForm,
      onHideModal: props.onHideModal,
      selectedPerson: selectedPerson,
      method: selectedPerson ? "PUT" : "POST"
    }
  }

  onHideModal = () => {
    this.state.onHideModal();
    this.setState({showModal: false});
  }
  
  componentWillReceiveProps(nextProps, _nextContext) {
    let nextState = {...nextProps};
    if (nextState.selectedPerson) {
      nextState.first_name = nextState.selectedPerson.first_name,
      nextState.last_name = nextState.selectedPerson.last_name,
      nextState.contact_number = nextState.selectedPerson.contact_number
    }
    this.setState(nextState);
  }

  onChangeField = (e, key) => {
    this.setState({[key]: e.target.value});
  }
  
  onSubmitForm = () => {
    this.state.submitForm(this.state, this.state.method)
    this.setState({showModal: false});
  }

  render() {
    return(
      <Modal show={this.state.showModal} onHide={() => this.onHideModal()}>
        <Modal.Header closeButton>
          <Modal.Title>Create Person</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <FieldGroup
              id="first_name"
              type="text"
              label="First Name"
              onChange={(e) => this.onChangeField(e, "first_name")}
              value={this.state.first_name}
              placeholder="Enter the person's first name here" />
            <FieldGroup
              id="last_name"
              type="text"
              label="Last Name"
              value={this.state.last_name}
              onChange={(e) => this.onChangeField(e, "last_name")}
              placeholder="Enter the person's last name here" />
            <FieldGroup
              id="contact_number"
              type="contact_number"
              label="Contact Number"
              value={this.state.contact_number}
              onChange={(e) => this.onChangeField(e, "contact_number")}
              placeholder="Enter the person's contact number here" />
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle="danger" onClick={() => this.onHideModal()}>Close</Button>
          <Button bsStyle="success" onClick={() => this.onSubmitForm()}>Submit</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

const FieldGroup = ({ id, label, ...props }) => {
  return (
    <FormGroup controlId={id}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...props} />
    </FormGroup>
  );
}


const mapStateToProps = ({ people }) => ({
  people: people.people,
  selectedPerson: people.selectedPerson,
  metadata: people.metadata,
  showModal: people.showModal
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      listPeopleNow,
      createPersonNow,
      showModalNow,
      hideModalNow,
      deletePersonNow,
      selectPersonNow
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)