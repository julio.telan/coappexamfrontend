import axios from "axios";
import { LIST_PEOPLE, GET_PERSON, API_BASE_URL, SHOW_MODAL_NOW, HIDE_MODAL_NOW, SELECT_PERSON } from "../constants";

export function listPeopleNow(page, perPage) {
    return (dispatch) => {
        dispatch(getPeople(page, perPage));
    }
}

export function createPersonNow(data, method) {
    return (dispatch) => {
        if (method === "POST") {
            dispatch(createPerson(data));
        } else {
            dispatch(editPerson(data));
        }
    }
}

export function showModalNow() {
    return dispatch => {
        dispatch({
            type: SHOW_MODAL_NOW
        })
    }
}

export function hideModalNow() {
    return dispatch => {
        dispatch({
            type: HIDE_MODAL_NOW
        })
    }
}

export function deletePersonNow(id) {
    return (dispatch) => {
        dispatch(deletePerson(id));
    }
}

export function selectPersonNow(data) {
    return (dispatch) => {
        dispatch(selectPerson(data));
    }
}

function selectPerson(data) {
    return {
        type: SELECT_PERSON,
        selectedPerson: data
    }
}

function deletePerson(id) {
    return (dispatch, getState) => {
        return axios({
            method: "DELETE",
            url: API_BASE_URL + '/person/' + id,
            timeout: 20000,
            responseType: "json"
        })
        .then((response) => {
            dispatch(listPeopleNow(1, 30));
        })
        .catch((error) => {
            alert(error);
        });
    }
}


function createPerson(data) {
    return (dispatch, getState) => {
        return axios({
            method: "POST",
            data: {
                first_name: data.first_name,
                last_name: data.last_name,
                contact_number: data.contact_number
            },
            url: API_BASE_URL + '/person',
            timeout: 20000,
            responseType: "json"
        })
        .then((response) => {
            dispatch(listPeopleNow(1, 30));
        })
        .catch((error) => {
            alert(error);
        });
    }
}

function editPerson(data) {
    return (dispatch, getState) => {
        return axios({
            method: "PUT",
            data: {
                first_name: data.first_name,
                last_name: data.last_name,
                contact_number: data.contact_number
            },
            url: API_BASE_URL + '/person/' + data.selectedPerson._id,
            timeout: 20000,
            responseType: "json"
        })
        .then((response) => {
            dispatch(listPeopleNow(1, 30));
        })
        .catch((error) => {
            alert(error);
        });
    }
}

function getPeople(page, perPage) {
    return (dispatch) => {
        return axios({
            method: "GET",
            params: {
                page,
                perPage,
            },
            url: API_BASE_URL + '/person',
            timeout: 20000,
            responseType: "json"
        })
        .then((response) => {
            const data = response.data;
            dispatch(listPeople(data.data, data.metadata));
        })
        .catch((error) => {
            alert(error);
        });
    }
}

function listPeople (people, metadata) {
    return {
        type: LIST_PEOPLE,
        people: people,
        metadata: metadata
    }
}